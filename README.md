## Name
HT16K33 7 Segment Adafruit

Code to drive the Adafruit 7 Segment HT16k33 I2C backpack:
https://learn.adafruit.com/adafruit-led-backpack/0-dot-56-seven-segment-backpack

Code based off the MAX7219 component and Samuel Sieb's ESPHome HT16k33 component:

## Description
This is a component for ESPHome.io to drive the Adafruit 7 Segment HT16k33 I2C backpack fitted with a 4 digit plus centre colon 7 segment display:

The default display is a multiplexed Luckylight KW4-56NCBA-P 4 digit, 4 DP, 1 centre colon display: https://cdn-shop.adafruit.com/datasheets/812datasheet.pdf

As displays vary slightly, especially with how the decimal points and colons are wired, I may include some mapping options to allow the user to choose alternal displays at run time.

## Visuals
TODO

## Installation
TODO

## Usage
TODO

## Support
Email me on tw_esphome@dionic.net

## Roadmap
1 - As displays vary slightly, especially with how the decimal points and colons are wired, I may include some mapping options to allow the user to choose alternal displays at run time.

## Contributing
Contributions welcome under the standard ESPHOME License (see LICENSE file in this repository)

## Authors and acknowledgment
Tim J Watts, England (C) 2021 <tw_gitlab@dionic.net>

Driving methods for the HT16K33 chip: Taken from Samuel Sieb's 14 segment ESPHome component https://github.com/ssieb/custom_components/tree/master/ht16k33_alpha which is a lot easier to get a quickstart on than the datasheet (https://cdn-shop.adafruit.com/datasheets/ht16K33v110.pdf) - although the datasheet was referenced for additional functionality.

The core code is based on the exisiting ESPHome MAX7219 component as it is a 7 segment display based component and I wanted the API to conform to an existing one people are used to, with additional functionality where possible.

Elements of code may have been taken from both projects above which share a common ESPHome license because we don't reinvent the wheel if possible.
## License
Standard ESPHome License as of 2021 - See LICENSE file in this repository for full text.

## Project status
Active development (as of October 2021)

